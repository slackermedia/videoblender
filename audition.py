#  ***** GPL LICENSE BLOCK *****
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  All rights reserved.
#  ***** GPL LICENSE BLOCK *****

bl_info = {
    "name": "Audition Clip",
    "author": "Klaatu",
    "version": (0,3,0),
    "blender": (2, 7, 0, 0),
    "category": "Sequencer",
    "location": "FileBrowser > UI > Audition Clip",
    "description": "Audition clips before you add them to the timeline",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "gitorious.org",}

import bpy
from subprocess import call

class AuditionPanelInit(bpy.types.Panel):
    """Audition a video clip before adding to timeline"""
    bl_label = "Audition"
    bl_idname = "audition.panel"
    bl_context = "object"
    bl_space_type = "FILE_BROWSER"
    bl_region_type = "CHANNELS"

    def draw(self, context):
        layout = self.layout
        layout.operator("audition.clip")
    
class AuditionButton(bpy.types.Operator):
    bl_idname = "audition.clip"
    bl_label = "Audition Clip"
    bl_space_type = "FILE_BROWSER"
    bl_region_type = "CHANNELS"
    #country = bpy.props.StringProperty()
 
    def execute(self,context):    
        for a in bpy.context.window.screen.areas:
            if a.type == 'FILE_BROWSER':
                params = a.spaces[0].params
                path = params.directory
                clip = params.filename
                
                print(path)
                print(clip)
                call(['ffplay', path + clip])
                break
            #return {'FINISHED'} 

        
def register():
    bpy.utils.register_class(AuditionPanelInit)
    bpy.utils.register_class(AuditionButton)

def unregister():
    bpy.utils.unregister_class(AuditionPanelInit)
    bpy.utils.unregister_class(AuditionButton)
      
if __name__ == "__main__":
    register()
    # make callable from scripts
    #bpy.ops.wm.call_menu(name=SimpleCustomMenu.bl_idname)
    bpy.utils.register_module(__name__)